%define entry 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %%next: dq entry
            db %1, 0
            %2:
            %define entry %%next
        %else
            %fatal "Неверный формат метки"
        %endif
    %else
        %fatal "Неверный формат ключа. Должна быть строка"
    %endif
%endmacro

