COMPILER=nasm
LD=ld
FLAGS=-felf64

main: main.o lib.o dict.o
	$(LD) -o $@ $^

lib.o: lib.asm
	$(COMPILER) $(FLAGS) -o $@ $<

dict.o: dict.asm lib.inc
	$(COMPILER) $(FLAGS) -o $@ $<

main.o: main.asm colon.inc words.inc lib.inc
	$(COMPILER) $(FLAGS) -o $@ $<

clean:
	rm -f *.o main

all: main

.PHONY: clean all

