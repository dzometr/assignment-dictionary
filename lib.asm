global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy

%define stdout 1
%define stderr 2

section .text
 
 
; Принимает код возврата и завершает текущий процесс
; Аргументы:		rdi - код возврата
exit: 
    mov rax, 60		; Пушим код системного вызова exit. Код возврата уже в rdi.
    syscall
 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; Аргументы:		rdi - указатель на нуль-термированную строку
; Возврат:		rax - длина строки
string_length:
    xor rax, rax
    .loop:
		cmp byte [rdi+rax], 0 ; Сравниваем с нуль-терминатором
	  	je .end 
	  	inc rax
		jmp .loop 		
	.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; Аргументы:		rdi - указатель на нуль-термированную строку 
print_string:
	push rdi			; Сохраняем в стек как caller-saved регистр
    call string_length
    mov rdx, rax
    pop rsi
	mov rax, 1			; Задаём параметр для системного вызова (write)
    mov rdi, stdout			; Задаём параметр для системного вызова (stdout)
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; Аргументы:		rdi - указатель на нуль-термированную строку 
print_error:
	push rdi			; Сохраняем в стек как caller-saved регистр
    call string_length
    mov rdx, rax
    pop rsi
	mov rax, 1			; Задаём параметр для системного вызова (write)
    mov rdi, stderr			; Задаём параметр для системного вызова (stderr)
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
    mov rsi, rsp			; Задаём адрес начала выводимой строки
	mov rax, 1			; Задаём параметр для системного вызова (write)
	mov rdx, 1			; Задаём длину выводимой строки
	mov rdi, stdout			; Задаём параметр для системного вызова (stdout)
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
	jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Аргументы:		rdi - указатель на число 
print_uint:
    mov rax, rdi
	mov r10, 10;			
    mov r9, rsp 			; Сохранение стека
    dec rsp
    mov byte [rsp], 0 		; Нуль-терминатор
    .loop:
    	xor rdx, rdx
      	div r10				; Делитель (10)
       	add rdx, 0x30 		; Превращаем в ASCII код
       	dec rsp
       	mov byte [rsp], dl
       	test rax, rax
       	jnz .loop 		; Следующая итерация
    mov rdi, rsp
    push r9
    call print_string	
    pop rsp		; Восстановление стека
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0 							
	jge print_uint		; Если >= 0, то сразу печатаем			
	push rdi 							
	mov rdi, '-' 						
	call print_char 	; Если < 0, то сначала печатаем минус			
	pop rdi 						
	neg rdi 							
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; Аргументы: 		rdi - указатель на первую строку, rsi - указатель на вторую
; Возврат:		rax
string_equals:
	xor rcx, rcx
    xor r8, r8
    .loop:
    	mov r8b, byte [rdi + rcx]
       	cmp r8b, byte [rsi + rcx]
       	jne .not_equals
       	inc rcx
       	cmp r8b, 0 
       	jne .loop
    .equals:
    	mov rax, 1
       	ret
    .not_equals:
       	xor rax, rax
       	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; Возврат:      rax - прочитанный символ, иначе 0
read_char:
	xor rax, rax 						
	xor rdi, rdi 		; Файловый дескриптор stdin				
	push rax		; Выделяем место на стеке 								
	mov rsi, rsp 		; Указатель на место записи символа				
	mov rdx, 1 		; Задаём длину считываемой строки				
	syscall 							
	pop rax 		; Помещаем символ в rax				
	ret

; Аргументы:		rdi - символ
; Возврат:      	rax - 1 если символ пробельный, иначе 0
is_whitespace:
	cmp rdi, ' ' 		; Пробел
    je .whitespace
    cmp rdi, '	' 	; Табуляция
    je .whitespace
    cmp rdi, '\n' 		; Перевод строки
    je .whitespace
    xor rax, rax
    ret
    .whitespace:
    	mov rax, 1
       	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; Аргументы: 		rdi - адрес начала буфера, rsi - размер буфера
; Возврат: 		rax - адрес буфера иначе 0, rdx - длина слова
read_word:
	push rdi
   	push rsi
    push 0 				; Длина слова
    .skip:				; Пропуск пробелов перед словом
    	call read_char
       	test rax, rax
       	jz .empty_word 		; Пустая строка
       	mov rdi, rax
       	call is_whitespace
       	cmp rax, 1
       	je .skip
    .read:
       	mov rax, rdi
       	pop rdx
       	pop rsi
       	pop rdi
       	cmp rdx, rsi
       	jge .error 		; Переполнение буфера
       	mov [rdi + rdx], rax
       	inc rdx
       	push rdi		; Caller-saved регистры
       	push rsi
       	push rdx
       	call read_char
       	test rax, rax
       	jz .end 		; Проверка на нуль-терминатор
       	mov rdi, rax
       	call is_whitespace
       	test rax, rax
       	jz .read 		; Продолжить чтение
    .end:
       	pop rdx			; Caller-saved регистры
       	pop rsi
       	pop rax
       	ret
    .empty_word:
       	pop rdx
       	pop rsi
       	pop rdi
    .error:
       	xor rax, rax
       	ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; Атрибуты:		rdi - указатель на строку
; Возврат:      	rax - прочитанное число, rdx - длина числа в символах, иначе 0
parse_uint:
	xor r8, r8
	xor rdx, rdx
    xor rax, rax
    .loop:
    	mov r8b, byte [rdi+rdx]
       	test r8, r8 		; Проверка на нуль-терминатор
       	jz .end
       	cmp r8, '0' 	
       	jb .end
       	cmp r8, '9' 	
       	ja .end
       	sub r8, 0x30 		; Превращаем в ASCII код
       	imul rax, 10
       	add rax, r8
       	inc rdx
       	jmp .loop
    .end:
    	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Атрибуты:		rdi - указатель на строку
; Возврат:      	rax - прочитанное число, rdx - длина числа в символах (включая знак, если он был), иначе 0
parse_int:
    xor rdx, rdx
    xor rax, rax
    cmp byte [rdi], '-'
    je .neg
    jmp parse_uint
    .neg:
    	inc rdi
       	call parse_uint
       	inc rdx
       	neg rax
       	ret
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Аттрибуты:    rdi - указатель на строку, rsi - указатель на буфер, rdx - длина буфера
; Возврат:      rax - длина строки, иначе 0

string_copy:
	xor rax, rax
	xor r10, r10
   	.loop:
       	cmp rax, rdx
		je .err 		; Переполнение буфера
		mov r10, [rdi + rax]
		mov [rsi + rax], r10
		test r10, r10
		jz .end ; конец строки
		inc rax
		jmp .loop
    .err:
       	xor rax, rax
    .end:
        ret
