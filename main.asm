%include "words.inc"
%include "lib.inc"

extern find_word

global _start

%define BUFFER_SIZE 256
%define RESULT_SUCCESS 0
%define RESULT_ERROR 1

section .rodata

welcome_message: db "Введите ключ: ", 0
success_message: db "Значение: ", 0
overflow_message: db "Ключ длиннее 256 символов", 0xA, 0
fail_message: db "Ключ не найден", 0xA, 0

section .text

_start:
	mov rdi, welcome_message
    call print_string

	sub rsp, BUFFER_SIZE 		; Выделяем место в стеке
    mov rdi, rsp			; Адрес начала буфера для сохранения ключа
    mov rsi, BUFFER_SIZE		; Размер буфера
    call read_word
    test rax, rax			
    jz .overflow 			; Ошибка чтения из-за переполнения буфера	
	
	mov rdi, rax			; Адрес начала буфера, в котором лежит ключ
    mov rsi, entry
    call find_word
    test rax, rax
    jz .fail 			; Ключ не найден

    .success:
		mov rdi, rax		; Адрес начала вхождения
		push rdi
		call string_length
		pop rdi
		add rdi, rax		; Пропускаем ключ и нуль-терминатор
		inc rdi
		push rdi			; Пушим адрес значения
       	mov rdi, success_message 
       	call print_string 	; Вывод текста об успехе
       	pop rdi				; Получаем обратно адрес значения
       	call print_string	; Выводим само значение
       	call print_newline
		add rsp, BUFFER_SIZE
		mov rdi, RESULT_SUCCESS
       	call exit
		   
   	.overflow:
       	mov rdi, overflow_message
       	call print_error
		jmp .error

    .fail:
       	mov rdi, fail_message
       	call print_error
		jmp .error

	.error:
		add rsp, BUFFER_SIZE
		mov rdi, RESULT_ERROR
       	call exit
		   
