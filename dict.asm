%include "lib.inc"

global find_word

%define ENTRY_OFFSET 8

section .text

; Аргументы: 	rdi - указатель на нуль-терминированную строку, rsi - указатель на начало map
; Возврат:   	rax - адрес начала вхождения в map, если найдено, иначе 0
find_word:
	.loop:
		test rsi, rsi
		jz .fail			; Ключ не найден
    	add rsi, ENTRY_OFFSET 		; Указатель на следующий элемент map
		push rsi		; Caller-saved регистры
		push rdi
   		call string_equals
		pop rdi
		pop rsi
   		test rax, rax
   		jnz .success 	; Ключ найден
   		mov rsi, [rsi - ENTRY_OFFSET]		; Указатель на следующий элемент map	
   		jmp .loop
	.fail:
   		xor rax, rax
   		ret
	.success:
   		mov rax, rsi
   		ret       

